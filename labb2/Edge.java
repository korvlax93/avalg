public class Edge{

  Node dest;
  int weight;

  public Edge(Node dest, int weight){
    this.dest = dest;
    this.weight = weight;
  }

}
