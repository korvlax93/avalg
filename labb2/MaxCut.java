import java.util.Random;
import java.util.HashMap;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

public class MaxCut{
  public HashMap<Integer, Node> parse(String fileName){
    HashMap<Integer, Node> nodes = new HashMap<>();
    String fs = File.separator;
    String path = System.getProperty("user.dir") + fs + fileName;
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(new File(path)));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    String currentLine = "";
    try {
      currentLine = br.readLine();
      for(int i = 1 ; i <= Integer.parseInt(currentLine.split(" ")[0]) ; i++) nodes.put(i, new Node(i));
      while ((currentLine = br.readLine()) != null){
        String[] fields = currentLine.split(" ");
        Edge e1 = new Edge(nodes.get(Integer.parseInt(fields[1])), Integer.parseInt(fields[2]));
        nodes.get(Integer.parseInt(fields[0])).addEdge(e1);
        Edge e2 = new Edge(nodes.get(Integer.parseInt(fields[0])), Integer.parseInt(fields[2]));
        nodes.get(Integer.parseInt(fields[1])).addEdge(e2);
      }
    } catch (IOException e1) {
      e1.printStackTrace();
    }
    return nodes;
  }
  public int algorithmR(HashMap<Integer, Node> nodes){
    Random rnd = new Random();
    HashMap<Integer, Node> partition = new HashMap<>();
    for(Node n : nodes.values())
      if(rnd.nextInt(2) == 1) partition.put(n.id, n);
    return maxCut(partition);
  }
  public int maxCut(HashMap<Integer, Node> partition){
    int sumCut = 0;
    for(Node n : partition.values()){
      for(Edge e : n.adj){
        if(!partition.values().contains(e.dest)) sumCut += e.weight;
      }
    }
    return sumCut;
  }
  public int algorithmS(HashMap<Integer, Node> nodes, HashMap<Integer, Node> preSetNodes){
    HashMap<Integer, Node> partition = new HashMap<>();
    if(preSetNodes != null) partition.putAll(preSetNodes);
    boolean unchanged = false;
    int currMax = 0;
    while(!unchanged){
      unchanged = true;
      for(Node n : nodes.values()){
        if(!partition.containsValue(n)){
          partition.put(n.id, n);
          int tempCut = maxCut(partition);
          if(tempCut > currMax)  {
            currMax = tempCut;
            unchanged = false;
          } else {
            partition.remove(n.id);
          }
        }else {
          partition.remove(n.id);
          int tempCut = maxCut(partition);
          if(tempCut > currMax)  {
            currMax = tempCut;
            unchanged = false;
          } else {
            partition.put(n.id, n);
          }
        }
      }
    }
    return currMax;
  }
  public int algorithmRS(HashMap<Integer, Node> nodes){
    Random rnd = new Random();
    HashMap<Integer, Node> partition = new HashMap<>();
    for(Node n : nodes.values())
      if(rnd.nextInt(2) == 1) partition.put(n.id, n);
    return algorithmS(nodes, partition);
  }

  public static void main(String[] args){
    MaxCut mc = new MaxCut();
    System.out.println("sumCut: " + mc.algorithmR(mc.parse("pw09_100.9.txt")));
    System.out.println("sumCut: " + mc.algorithmR(mc.parse("matching_1000.txt")));
    System.out.println("sumCut: " + mc.algorithmS(mc.parse("pw09_100.9.txt"), null));
    System.out.println("sumCut: " + mc.algorithmS(mc.parse("matching_1000.txt"), null));
    System.out.println("sumCut: " + mc.algorithmRS(mc.parse("pw09_100.9.txt")));
    System.out.println("sumCut: " + mc.algorithmRS(mc.parse("matching_1000.txt")));


  }
}
