#!/usr/bin/python
import sys
import random
import numpy as np
def checkConnectivity(s, t, towns, visited):
    if s==t:
        return True
    for i in towns[s]:
        if not visited[i[0]]:
            visited[i[0]] = True;
            if checkConnectivity(i[0], t, towns, visited):
                return True
def simulate(companyHQ, cycles, townlist, home):
    sum = 0
    i=companyHQ
    for k in range(cycles):
        while i != home:
            rnd = random.random()
            for j in townlist[i]:
                if rnd<j[2]:
                    sum += j[1]
                    i=j[0]
                    break
                rnd -= j[2]
        i = companyHQ
    print sum/cycles
    return sum/cycles
def parse():
    line = sys.stdin.readline()
    N, M, H, F, P = map(int, line.split())
    towns = list()
    for i in range(N):
        towns.append(list())
    for i in range(M):
        line = (sys.stdin.readline()).split()
        U, V, T, P_uv, P_vu = int(line[0]), int(line[1]), float(line[2]), float(line[3]), float(line[4])
        towns[U].append((V, T, P_uv))
        towns[V].append((U, T, P_vu))
    return (towns, H, F, P)
def stationary_distribution(H, towns):
    b = np.array(np.zeros(len(towns)))
    A = list()
    for i in range(len(towns)):
        A.append([0]*len(towns))
    for i in range(len(b)):
        for j in towns[i]:
            b[i] -= j[1]*j[2]
    for i in range(len(A)):
        for j in towns[i]:
            A[i][j[0]] = j[2]
    A = np.subtract(A, np.identity(len(A)))
    if(np.linalg.det(A) != 0):
        x = np.linalg.solve(A, b)
        return (x, 1)
    else:
        print("no solution for the system (non markovian)")
        return (0, -1)

towns, H, F, P = parse()
visited = [False]*len(towns)
boolF = checkConnectivity(F, H, towns, visited)
visited = [False]*len(towns)
boolP = checkConnectivity(P, H, towns, visited)
emp_F = 0.0
emp_P = 0.0
if boolF:
    print("Avg(F) of MonteCarlo: ")
    emp_F = simulate(F, 10000, towns, H)
else: print("no way for Fedups to reach home")
if boolP:
    print("Avg(P) of MonteCarlo: ")
    emp_P = simulate(P, 10000, towns, H)
else: print("no way for postNHL to reach home")
print("Stationary time distribution depending on starting state:")
solution = stationary_distribution(H, towns)
if not solution[1]==-1:
    print(solution[0][F],solution[0][P])
    print("Relative error F and P: ")
    print(abs((solution[0][F] - emp_F)/solution[0][F]),abs((solution[0][P] - emp_P)/solution[0][P]))
print("-----------------------------------------------------\n-----------------------------------------------------\n-----------------------------------------------------")
