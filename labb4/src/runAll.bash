#! /usr/bin/env zsh
echo 'rnd1'
./montecarlo.py < ../data/rnd1.in;
echo 'rnd2'
./montecarlo.py < ../data/rnd2.in;
echo 'rnd3'
./montecarlo.py < ../data/rnd3.in;
echo 'small'
./montecarlo.py < ../data/small.in;
echo 'toy'
./montecarlo.py < ../data/toy.in;
echo 'strange1'
./montecarlo.py < ../data/strange1.in;
echo 'strange2'
./montecarlo.py < ../data/strange2.in;
