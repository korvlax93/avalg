import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
public class MaxIndSet{
  ArrayList<Node> graph;
  ArrayList<Node> tree;
  ArrayList<ArrayList<Node>>
  Random rnd;
  Node startRoot;
  public MaxIndSet(String file_graph, String file_decomp){
    graph = new ArrayList<>();
    tree = new ArrayList<>();
    graph.add(null);
    tree.add(null);
    parse(file_graph);
    parse(file_decomp);
    rnd = new Random();
    //root = tree.get(rnd.nextInt(tree.size()) + 1);
    startRoot = tree.get(1);
  }

  public void r0(Node root, short ){
    for(int i = 0 ; i < root.adj.size() ; i++){
      if(root.adj.size() == 0){
        //leaf
        long subSet = 0b0;
        for(int i = 0 ; i < Math.pow(2,root.v_i.size()+1) ; i++){
          subSet += 0b1;
          HashSet<Node> U = new HashSet();
          for(int i = 0; i<64; i++){
            if(subSet & 1 == 1) U.add(bag.v_i.get(i));
            subSet >> 1;
          }
          if(isIndependent(U, root)){
            root.tree_table[subSet][0] = 1;
            root.tree_table[subSet][1] = Long.bitCount(subSet);
          }
        }
      }else{
        //internal node
        long subSet = 0b0;
        for(int i = 0 ; i < Math.pow(2,root.v_i.size()+1) ; i++){
          subSet += 0b1;
          short sum  = Long.bitCount(subSet);
          HashSet<Node> uSet = new HashSet();
          for(int k = 0; k<64; k++){
            if(subSet & 1 == 1) U.add(bag.v_i.get(k));
            subSet >> 1;
          }
          if(isIndependent(uSet, root)){
            root.tree_table[subSet][0] = 1;
            for(int j = 0; j < root.adj.size(); j++){
              HashSet<Node> u_vt = new HashSet(uSet);
              u_vt.retainAll(root.get(k));
              sum += findMax(uSet, u_vt, root, root.adj.get(j));
            }
          }
        r0(root.adj.get(i));
      }
    }
  }
  public short findMax(HashSet<Node> U, HashSet<Node> u_vt, Node v_t, Node v_ti){

  }
  public boolean isIndependent(HashSet<Node> chosen_nodes, Node bag){
    for(Node n : chosen_nodes){
      for(Node m : n.adj){
        if(chosen_nodes.contains(m))
                            return false;
      }
    }
    return true;
  }
  public void parse(String fileName) {
    String fs = File.separator;
    String path = System.getProperty("user.dir") + fs + "data" + fs + fileName;
    System.out.println(path);
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(new File(path)));
    } catch (FileNotFoundException e) {
      System.out.println("No such file");
      return;
    }
    String currentLine = "";
    try {
      boolean is_graph=false;
      while ((currentLine = br.readLine()) != null){
        String[] fields = currentLine.split(" ");
        switch(fields[0]) {
          case "c":
            break;
          case "p":
            is_graph = true;
            for(int i = 1; i <= Integer.parseInt(fields[2]); i++)
                  graph.add(new Node(i));
            break;
          case "s":
            for(int i = 1; i<=Integer.parseInt(fields[2]); i++)
                        tree.add(new Node(i));
            for(Node n : tree){
              if(n!=null)
                  n.initializeTable(Integer.parseInt(fields[3]));
            }
            break;
          case "b":
            int b = Integer.parseInt(fields[1]);
            for (int i = 2; i<fields.length; i++){
              tree.get(b).addToBag(graph.get(Integer.parseInt(fields[i])));
            }
            break;

          default:
            if(is_graph){
              graph.get(Integer.parseInt(fields[0]))
                    .addConnection(graph.get(Integer.parseInt(fields[1])));
              graph.get(Integer.parseInt(fields[1]))
                    .addConnection(graph.get(Integer.parseInt(fields[0])));
            }else{
              tree.get(Integer.parseInt(fields[0]))
                  .addConnection(tree.get(Integer.parseInt(fields[1])));
            }


        }
      }
    } catch (IOException e1) {
      e1.printStackTrace();
    }
  }
  private class Node{
    int id;
    ArrayList<Node> adj;
    HashMap<Node> v_i;
    short[][] tree_table;

    public Node(int id){
      adj = new ArrayList<>();
      v_i = new HashSet<>();
      this.id = id;
    }
    public void addConnection(Node neibr){
      adj.add(neibr);
    }
    public void addToBag(Node node){
      v_i.put(v_i.values().size(), node);
    }
    public void initializeTable(int wPlusOne){
      tree_table = new short[(short)) Math.pow(2,wPlusOne)][2];
    }
    public String toString() {
      return "" + id;
    }
  }
  public static void main(String[] args){
    MaxIndSet m = new MaxIndSet(args[0]+".gr", args[0]+".td");

    System.out.println("-----------Graph------------");
    for(Node n : m.graph){
      if(n==null) continue;
      System.out.print(n.toString() + " has neighbours: ");
      for(Node k : n.adj){
        System.out.print(k.toString() + ", ");
      }
      System.out.println("");
    }
    System.out.println("-----Tree Decomposition-----");
    for(Node n : m.tree){
      if(n==null) continue;
      System.out.print(n.toString() + " has children: ");
      for(Node k : n.adj){
        System.out.print(k.toString() + ", ");
      }
      System.out.println("");
      System.out.print(n.toString() + " has nodes: ");
      for(Node k : n.v_i){
        System.out.print(k.toString() + ", ");
      }
      System.out.println("");
    }

    m.r0(startRoot);
  }
}
