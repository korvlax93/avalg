#!/usr/bin/python3
import sys
import Node as n
def parse_graph(fileName):
    f = open(fileName,"r")
    graph = list()
    graph.append(None)
    for line in f:
        fields = line.split()
        if fields[0] == "c":
            continue
        if fields[0] == "p":
            V = int(fields[2])
            E = int(fields[3])
            for i in range(1,V+1):
                node = n.Node(i)
                graph.append(node)
            print(V)
            continue
        source = graph[int(fields[0])]
        dest = graph[int(fields[1])]
        source.add_connection(dest)
        dest.add_connection(source)
    return graph

def parse_tree(fileName, graph):
    f = open(fileName, "r")
    tree = list()
    tree.append(None)
    for line in f:
        fields = line.split()
        if fields[0] == "c":
            continue
        elif fields[0] == "s":
            B = int(fields[2])
            w = int(fields[3])-1
            V = int(fields[4])
            for i in range(1,B+1):
                node = n.Node(i)
                tree.append(node)
            print(w)
            continue
        elif fields[0] == "b":
            b = int(fields[1])
            for j in range(2,len(fields)):
                tree[b].add_to_bag(graph[int(fields[j])])
            tree[b].sort_ordered_bag()
            tree[b].init_table()
            continue
        else:
            tree[int(fields[0])].add_connection(tree[int(fields[1])])
            tree[int(fields[1])].add_connection(tree[int(fields[0])])
    return tree

def r0(t):
    if t == None: return
    for child in t.adj:
        child.remove_connection(t)
        r0(child)
    if len(t.adj) == 0:#leaf
        for subset in range(pow(2,len(t.bag))):
            choosen_nodes = convert_bits_to_set(subset, t)
            if(is_indep(choosen_nodes)):
                #print("leaf we are: " + str(t))
                #print("U: ")
                #for s in choosen_nodes:
                #    print(str(s))
                t.table[subset] = len(choosen_nodes)
                #print("our value: " + str(t.table[subset]) + "\n")
            else:
                t.table[subset] = -1
    else:#not leaf
        for subset in range(pow(2,len(t.bag))):
            choosen_nodes = convert_bits_to_set(subset,t)
            if(is_indep(choosen_nodes)):
                #print("inter we are: " + str(t))
                #print("U: ")
                #for s in choosen_nodes:
                #    print(str(s))
                t.table[subset] = len(choosen_nodes) + findMax(choosen_nodes, t)
                #print("our value: " + str(t.table[subset]) + "\n")
            else:
                t.table[subset] = -1


def findMax(choosen_nodes, t):
    sum = 0
    for child in t.adj:
        sum += maxUi(choosen_nodes, t, child)
    return sum

def maxUi(choosen_nodes, t, child):
    max = 0
    for i in range(pow(2,len(child.bag))):
        if child.table[i] != -1:
            Ui = convert_bits_to_set(i, child)
            #print("we are looking at Ui for child: " + str(child))
            #for s in Ui:
            #    print(str(s))
            #print()
            if Ui.intersection(t.bag) == choosen_nodes.intersection(child.bag):
                curr = child.table[i] - len(Ui.intersection(choosen_nodes))
                if max < curr:
                    max = curr
            #    print("current max: " + str(max))
    return max

def is_indep(choosen_nodes):
    for node in choosen_nodes:
        for node2 in choosen_nodes:
            if node.has_connection(node2) and node is not node2:
                return False
    return True

def convert_bits_to_set(subset,t):#subset is a number
    choosen_nodes = set()
    for i in range(len(t.ordered_bag)):#converts bitstring to nodes in set...
        if subset & 1 == 1:
            choosen_nodes.add(t.ordered_bag[len(t.ordered_bag) - 1 - i])#assuming MSB is on right side
        subset = subset >> 1
    return choosen_nodes

graph = parse_graph("../../data/TaylorTwographDescendantSRG_3.gr")
tree = parse_tree("../../data/TaylorTwographDescendantSRG_3.td",graph)
"""
for i in range(1,len(graph)):
    node = graph[i]
    print (str(node) + "\nWith neibr")
    for j in node.adj:
        print (j)
    print ("\n")

print ("-------------TREE DECOMP------------")
for i in range(1,len(tree)):
    node = tree[i]
    print (str(node) + "\nWith neibr")
    for j in node.adj:
        print(j)
    print ("\nWith bag")
    for j in node.bag:
        print(j)
    print ("\n")
print ("Parsing done")
"""

r0(tree[1])
total_max = 0
#print ("tables for every node in tree")
for i in range(1, len(tree)):
        print (str(tree[i]) + " -> " + str(tree[i].table))
for i in range(len(tree[1].table)):
    if tree[1].table[i] > total_max:
        total_max = tree[1].table[i]
print("total max: " + str(total_max))
