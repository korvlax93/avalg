class Node:
    def __init__(self, id):
        self.id = id
        self.adj = set()
        self.bag = set()
        self.ordered_bag = list()
        self.visited = False
        #self.table = dict()

    def add_connection(self, dest):
        self.adj.add(dest)
    def remove_connection(self, t):
        self.adj.remove(t)
    def has_connection(self, dest):
        return dest in self.adj
    def add_to_bag(self,n):
        self.bag.add(n)
    def sort_ordered_bag(self):
        self.ordered_bag.extend(self.bag)
        self.ordered_bag.sort()
    def init_table(self):
        self.table = dict.fromkeys(range(pow(2,len(self.bag))))
    def __str__(self):
        return "ID: " + str(self.id)
    def __lt__(self, other):
        return self.id < other.id
