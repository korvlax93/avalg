import os
import MaxIndSet2 as mis
import time

def parse(fileName):
    f = open(fileName, "r")
    for line in f:
        fields = line.split()
        if fields[0] == "s":
            return (int(fields[3]), int(fields[4]))

print ("hej")
max_w = 0
max_n = 0
wfile = "non"
nfile = "non"
all_files = os.listdir("../../data")
file_list_w = list()
file_list_n = list()
for s in all_files:
    if s[int(len(s)-3):int(len(s))] == ".td":
        (w, n) = parse("../../data/" + s)
        file_list_w.append((w,s))
        file_list_n.append((n,s))
        #print(w, n, s)

file_list_w.sort(key = lambda tup : tup[0])
file_list_n.sort(key = lambda tup : tup[0])
print (len(file_list_w))
#print (file_list_w)

for i in range(1,len(file_list_w)):#skip empty graph...
    #print(file_list_w[i])
    #print (i)
    fileName = file_list_w[i][1]
    fileName_graph = fileName[0:int(len(fileName)-3)] + ".gr"
    #print (fileName)
    #print (fileName_graph)

    graph = mis.parse_graph("../../data/" + fileName_graph)
    tree = mis.parse_tree("../../data/" + fileName ,graph)

    start = time.time()
    mis.r0(tree[1])
    total_max = 0
    for i in range(len(tree[1].table)):
        if tree[1].table[i] > total_max:
            total_max = tree[1].table[i]
    end = time.time()
    print ("for file: " + fileName + "with w = " + str(file_list_w[i][0]) + " , n = " + str(len(graph)))
    print("we have total max: " + str(total_max) + "\n")
    if end - start > 60.0:
        print("max w for us is: " + file_list_w[0])
        break
    print ("time : " + str(end - start))
