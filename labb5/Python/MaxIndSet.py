#!/usr/bin/python
import sys
import Node
def parse_graph(fileName):
    f = open(fileName,"r")
    graph = list()
    graph.append(None)
    for line in f:
        fields = line.split()
        if fields[0] == "c":
            continue
        if fields[0] == "p":
            V = int(fields[2])
            E = int(fields[3])
            for i in range(1,V+1):
                n = Node.Node(i)
                graph.append(n)
            print(V,E)
            continue
        source = graph[int(fields[0])]
        dest = graph[int(fields[1])]
        source.add_connection(dest)
        dest.add_connection(source)
    return graph

def parse_tree(fileName, graph):
    f = open(fileName, "r")
    tree = list()
    tree.append(None)
    for line in f:
        fields = line.split()
        if fields[0] == "c":
            continue
        elif fields[0] == "s":
            B = int(fields[2])
            w = int(fields[3])-1
            V = int(fields[4])
            for i in range(1,B+1):
                n = Node.Node(i)
                tree.append(n)
            print(B,w,V)
            continue
        elif fields[0] == "b":
            b = int(fields[1])
            for j in range(2,len(fields)):
                tree[b].add_to_bag(graph[int(fields[j])])
            tree[b].sort_ordered_bag()
            tree[b].init_table()
            continue
        else:
            tree[int(fields[0])].add_connection(tree[int(fields[1])])

    return tree

#fills table for each node
def r0(t):
    #for each node t in T Post-roder, use stack...
    #print " at" + str(t) + " adjLength = " + str(len(t.adj))
    if t == None: return
    for child in t.adj:
        #print " with child" + str(child)
        r0(child)
    if len(child.adj) == 0:#leaf
        print "Entered child" + str(child)
        #iterate all subset of V_t which are indep
        for subset in range(pow(2,len(child.bag))):
            (indep, subset_size) = is_indep(subset,child)
            if(indep):
                print str(subset) + "is indep!"
                child.table[subset] = subset_size #wher U indep
        print "child" + str(child) + " Filled Table: " + str(child.table) + "\n"
    else:
        print "Entered intenal node : " + str(child)
        for subset in range(pow(2,len(child.bag))):
            indep, subset_size = is_indep(subset,child)
            if(indep):
                print str(subset) + "is indep!"
                subset_to_set = convert_bits_to_set(subset,child)
                child.table[subset] = findMax(child, subset_to_set)# - size of intersection
        #print "child" + str(child) + " Filled Table: " + str(child.table) + "\n"


def root_max(root):
    total_max = -1;
    for subset in range(pow(2,len(root.bag))):
        indep, subset_size = is_indep(subset,root)
        if(indep):
            if child.table[subset] > total_max:
                total_max = root.table[subset]
    return total_max


def findMax(t,subset):
    #sum_list = list()
    max2 = -1
    #subset_to_set = convert_bits_to_set(subset, t)
    for child in t.adj:
        U_i_list = find_subsets_i(t.bag, child.bag, subset,child, t)
        sum_list = [0]*len(U_i_list)
        print "U_i_list:"
        for i in range(len(U_i_list)):
            for j in U_i_list[i]:
                print ", " + str(j)
            print "\n"
        if child.adj == 0:
            max1 = -1
            for i in range(len(U_i_list)):
                if child.table[U_i_list[i]] > max1:
                    max1 = child.table[U_i_list[i]]
            return max1;
        else:
            for i in range(len(U_i_list)):
                sum_list[i] += len(subset) + findMax(child, U_i_list) - len(subset.intersection(U_i_list[i]))#there is more than one U_i .....
            for i in range(len(sum_list)):
                if sum_list[i] > max2: max2 = sum_list[i]

    return max2

def find_subsets_i(parent_bag, child_bag, parent_subset, child, parent):
    U_intersect_V_ti = parent_subset.intersection(child_bag)
    print "U_intersect_V_ti = " + ', '.join(str(s) for s in U_intersect_V_ti)
    print "\n"
    list_of_subsets = list()
    for U_i in range(pow(2,len(child_bag))):
        U_i_set = convert_bits_to_set(U_i, child)
        #print "V_t = " + ', '.join(str(s) for s in parent_bag) + " U_i_set = " + ', '.join(str(s) for s in U_i_set) + " V_t_intersect_U_i = " + " ,".join(str(s) for s in parent_bag.intersection(U_i_set))
        if parent_bag.intersection(U_i_set) == U_intersect_V_ti and is_indep(U_i, child)[0]:
            #print "Entered"
            list_of_subsets.append(U_i_set)
    #print "\n"
    return list_of_subsets

def is_indep(subset, t):
    choosen_nodes = convert_bits_to_set(subset,t)
    #print str(subset) + "yields the set " + ', '.join(str(s) for s in choosen_nodes)
    for node in choosen_nodes:
        for node2 in choosen_nodes:
            if node.has_connection(node2) and node is not node2:
                return False, -1
    return (True , len(choosen_nodes))

def convert_bits_to_set(subset,t):
    choosen_nodes = set()
    #print "now in bit conversion, we have ordered_bag = " + ', '.join(str(s) for s in t.ordered_bag)
    for i in range(len(t.ordered_bag)):#converts bitstring to nodes in set...
        if subset & 1 == 1:
            choosen_nodes.add(t.ordered_bag[len(t.ordered_bag) - 1 - i])#assuming MSB is on right side
        subset = subset >> 1
    return choosen_nodes




graph = parse_graph("../data/web1.gr")
for i in range(1,len(graph)):
    node = graph[i]
    print str(node) + "\nWith neibr"
    for j in node.adj:
        print j
    print "\n"
tree = parse_tree("../data/web1.td",graph)
print "-------------TREE DECOMP------------"
for i in range(1,len(tree)):
    node = tree[i]
    print str(node) + "\nWith neibr"
    for j in node.adj:
        print j
    print  "\nWith bag"
    for j in node.bag:
        print j
    print "\n"
print "Parsing done"

root = tree[1]# index 0 i null
print(r0(root))
print "tables for every node in tree"
for i in range(1, len(tree)):
        print str(tree[i]) + " -> " + str(tree[i].table)
print "RootMax: "
print(root_max(root))
