import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
public class MaxIndSet{
    int[][] adj = null;
    int count = 0;
    int nbrCalls = 0;
    public  void parse(String fileName){
      String fs = File.separator;
      String path = System.getProperty("user.dir") + fs + "data" + fs + fileName;
      System.out.println(path);
      BufferedReader br = null;
      try {
        br = new BufferedReader(new FileReader(new File(path)));
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      }
      String currentLine = "";
      int[][] adj = null;
      try {
        currentLine = br.readLine();
        int V = Integer.parseInt(currentLine);
        adj = new int[V][V];
        int i = 0;
        while ((currentLine = br.readLine()) != null){
          String[] fields = currentLine.split(" ");
          for(int j = 0 ; j < V ; j++) adj[i][j] = Integer.parseInt(fields[j]);
          i++;
        }
      } catch (IOException e1) {
        e1.printStackTrace();
      }
       this.adj=adj;
    }
  public int R0(int[] removed){
    boolean flag = true;
    int[] rmNeibr, rmSelf;
    int maxSum = 0, maxIndex = 0;
    rmNeibr = Arrays.copyOf(removed,removed.length);
    rmSelf = Arrays.copyOf(removed,removed.length);
    //for(int k=0; k<removed.length; k++) System.out.print(" " + removed[k]);
    //System.out.println("\n------------------");

    //is graph empty
    for(int i=0; i<removed.length; i++){
      if(removed[i]==0) flag = false;
    }
    if(flag) {
      System.out.println(count);
      return 0;
    }


    //r2
    /*
    for(int i=0; i<removed.length; i++){
      int twosum=0, u = 0, w = 0;
      boolean one= false, two=false;
      if(removed[i] == 0){
        for(int j=0; j<removed.length; j++){
          if(removed[j] == 0){
            twosum += adj[i][j];
            if(twosum == 1 && !one) {
              u = j;
              one = true;
            }
            if(twosum == 2 && !two) {
              w = j;
              two = true;
            }
          }
        }
      }
      if(twosum == 2){
        if(adj[u][w] == 1)
          return 1+R0(removeNeibr(rmNeibr, i));
        //else extend a node
        count++;
        //printAdj();
        extend(u, w, i);
        //printAdj();
        rmNeibr = Arrays.copyOf(removed,removed.length+1);
        return 1+R0(removeNeibr(rmNeibr, i));
      }

    }
    */




    //r1
    for(int i=0; i<removed.length; i++){
      int onesum=0;
      if(removed[i] == 0){
        for(int j=0; j<removed.length; j++){
          if(removed[j] == 0){
            onesum += adj[i][j];
          }
        }
      }
      if(onesum == 1){
        return 1+R0(removeNeibr(rmNeibr, i));
      }
    }



    //find lonely node
    for(int i=0; i<removed.length; i++){
      int sum=0;
      int rowSum=0;
      if(removed[i] == 0){
        for(int j=0; j<removed.length; j++){
            if(removed[j]==0){
              rowSum += adj[i][j];
              sum += adj[i][j];
            }
        }
        if(sum==0){
          removed[i] = 1;
          return 1 + R0(removed);
        }
        if(maxSum < rowSum){
          maxSum = rowSum;
          maxIndex = i;
        }
      }
      //System.out.println("\nsum:" + sum);
    }

    //remove self
    rmSelf[maxIndex] = 1;
    return Math.max(1+R0(removeNeibr(rmNeibr, maxIndex)), R0(rmSelf));
  }
  public void extend(int u, int w, int v){
    int[][] newAdj = new int[adj.length + 1][adj.length + 1];
    for(int i = 0 ; i < newAdj[0].length - 1 ; i++){
        for(int j = 0 ; j < newAdj[0].length - 1 ; j++){
          newAdj[i][j] = adj[i][j];
        }
        //connects z to neighbours of u and w
        if((adj[u][i] == 1 || adj[w][i] == 1) && v != i){
          newAdj[i][newAdj[0].length-1] = 1;
          newAdj[newAdj[0].length-1][i] = 1;
        }
    }
    this.adj = newAdj;
  }
  public int[] removeNeibr(int[]  inV, int u){
    for(int i = 0; i<inV.length; i++){
      if(adj[u][i]==1) inV[i]=1;
    }
    inV[u] = 1;
    //printArray(inV);
    return inV;
  }
  public void printAdj(){
    for(int i = 0 ; i< adj[0].length ; i++){
      for(int j = 0 ; j < adj[0].length ; j++){
        System.out.print(adj[i][j] + " ");
      }
      System.out.println();
    }
  }
  public void printArray(int[] a){
    for(int i  = 0 ;i < a.length ; i++){
      System.out.print(a[i] + " , ");
    }
    System.out.println();
  }
  public static void main(String[] args){
    MaxIndSet mis = new MaxIndSet();
    mis.parse("g60.in");
    int[] init = new int[mis.adj[0].length];
    System.out.println(mis.R0(init));
  }
}
