public class Edge{
  Node dest;
  public Edge(Node dest){
    this.dest = dest;
  }
  public String toString(){
    return "" + dest.id;
  }
  @Override
  public boolean equals(Object o){
    if (o == this) return true;
    if (!(o instanceof Node)) {
      return false;
    }
    Node n = (Node) o;
    return n.id == dest.id;
  }
}
