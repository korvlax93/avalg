public class Test{
  public static void main(String[] args){
    Node n1 = new Node(1);
    Node n2 = new Node(2);
    Node n3 = new Node(3);

    Edge e1 = new Edge(n3);
    Edge e2 = new Edge(n3);
    //Edge e3 = new Edge();

    n1.addEdge(e1);
    n2.addEdge(e2);

    Node maxDegNode = n1;//O(n) , Deg = Degree of node -> #Edges attached to Node
    e1.dest.adj.remove(maxDegNode);
    System.out.println(n3);

  }
}
