import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
public class MaxIndSet{
  public Graph parse(String fileName){
    String fs = File.separator;
    String path = System.getProperty("user.dir") + fs + "data" + fs + fileName;
    System.out.println(path);
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(new File(path)));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    String currentLine = "";
    Graph g = null;
    try {
      currentLine = br.readLine();
      int V = Integer.parseInt(currentLine);
      g = new Graph(V);
      int i = 0;
      while ((currentLine = br.readLine()) != null){
        String[] fields = currentLine.split(" ");
        for(int j = 0 ; j < V ; j++){
          if(Integer.parseInt(fields[j]) == 1) g.addEdge(i, j);
        }
        i++;
      }
    } catch (IOException e1) {
      e1.printStackTrace();
    }
    return g;
  }

  public int R0(Graph g){
    System.out.println("inGraphCount : " + g.inGraphCount);
    if(g.inGraphCount == 0){
      System.out.println("Passed EMPTY");
      return 0;
    }
    int alone = g.isolated();
    if(alone != -1){
      System.out.println("ISOLATED NODE FOUND: " + alone);
      g.removeIsolated(alone);
      return 1 + R0(g);
    }
    else{
      int maxNode = g.findMax();
      System.out.println("MAXNODE: " + maxNode);
      System.out.println("GRAPH STATE BEFORE REMOVAL: " + "\n" + g.toString());
      ArrayList<Restore> verticesToAddBack = g.removeUAndAdj(maxNode);//Restore stores vertex id and degree
      System.out.println("GRAPH STATE AFTER REMOVAL: " + "\n" + g.toString());
      int leftMax = 1 + R0(g);
      //add back the removed vertices
      g.addBack(verticesToAddBack);
      System.out.println("GRAPH STATE AFTER LEFTMAX: " + "\n" + g.toString());
      g.removeU(maxNode);
      int rightMax = R0(g);
      return Math.max(leftMax, rightMax);
    }


  }

  public static void main(String[] args){
    MaxIndSet mis = new MaxIndSet();
    Graph g = mis.parse("g5.in");
    //System.out.println(g);
    System.out.println("Max int set is " + mis.R0(g));
  }
}
