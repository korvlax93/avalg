import java.util.LinkedList;
public class Vertex{
  int id;
  LinkedList<Vertex> adj;
  int nrNeighbours;
  public Vertex(int id){
    this.id = id;
    adj = new LinkedList<Vertex>();
  }
  public void addNeighbour(int id, Vertex[] vertices){
    adj.add(vertices[id]);
  }
  public String toString(){
    return "" + id;
  }
}
