import java.util.ArrayList;
import java.util.LinkedList;
public class Graph{
  int V; //nr of vertices
  Vertex[] vertices;
  boolean[] inGraph;
  int inGraphCount;
  public Graph(int V){
    this.V = V;
    inGraphCount = V;
    vertices = new Vertex[V];
    inGraph = new boolean[V];
    for(int i = 0 ; i < V ; i++) inGraph[i] = true;
    for(int i = 0 ; i < V ; i++) vertices[i] = new Vertex(i);
  }
  public void addEdge(int source, int dest){
    vertices[source].addNeighbour(dest, vertices);
    vertices[source].nrNeighbours++;
  }

  public int isolated(){
    for(int i = 0; i < V ; i++){
      if(inGraph[i]){
        if(vertices[i].nrNeighbours == 0){
          //inGraph[i] = false;
          //inGraphCount--;
          return i;
        }
      }
    }
    return -1;//should return verticesToAddBack here aswell??
  }
  public int findMax(){
    int max = 0;
    int maxNode = -1;
    for(int i = 0 ; i < V ; i++){
      if(inGraph[i]){
        if(max < vertices[i].nrNeighbours){
          max = vertices[i].nrNeighbours;
          maxNode = i;
        }
      }
    }
    return maxNode;
  }

  public void removeIsolated(int isolated){
    inGraph[isolated] = false;
    inGraphCount--;
  }

  public void removeU(int u){
    Vertex v = vertices[u];
    for(Vertex w : v.adj){
      w.nrNeighbours--;
    }
    inGraph[u] = false;
    inGraphCount--;
    //decrement neighbours? already does this?
  }
  public ArrayList<Restore> removeUAndAdj(int u){
    ArrayList<Restore> verticesToAddBack = new ArrayList<Restore>();
    Vertex z = vertices[u];
    LinkedList<Vertex> adj = z.adj;

    //Kolla så detta funkar med exemplet från labbhandledningen...

    //ta bort u
    verticesToAddBack.add(new Restore(z.id, z.nrNeighbours));
    System.out.println(z.id + " , " + z.nrNeighbours);
    inGraph[u]= false;
    inGraphCount--;
    z.nrNeighbours = 0;

    //ta bort grannar till u
    for(Vertex v : adj){
      if(inGraph[v.id]){
        System.out.println("A");
        System.out.println(v.id + " , " + v.nrNeighbours);

        //ta bort edges som pekade på grannar till u
        for(Vertex w : v.adj){
          if(inGraph[w.id] && !adj.contains(w)){
            for(Vertex q : w.adj){
              if(q.id == v.id && inGraph[q.id]){
                System.out.println("B");//Something wrong here
                System.out.println(q.id + " , " + q.nrNeighbours);
                verticesToAddBack.add(new Restore(q.id, q.nrNeighbours));
                q.nrNeighbours--;
              }
            }
          }

        }
        verticesToAddBack.add(new Restore(v.id, v.nrNeighbours));
        inGraph[v.id] = false;
        inGraphCount--;
        //vertices[v.id].nrNeighbours = 0;
        v.nrNeighbours = 0;
      }
    }
    return verticesToAddBack;
  }
  public void addBack(ArrayList<Restore> verticesToAddBack){
    for(int i = 0; i < verticesToAddBack.size() ; i++){
      Restore r = verticesToAddBack.get(i);
      if(inGraph[r.id] == false){
        inGraph[r.id] = true;
        inGraphCount++;
      }
      System.out.println(r.id + " , " + r.nrNeighbours);
      vertices[r.id].nrNeighbours = r.nrNeighbours;
    }
  }
  public String toString(){
    StringBuilder sb = new StringBuilder();
    for(Vertex v : vertices){
      sb.append("ID: " + v.id + " Adj: " + v.adj + " NrAdj: " + v.nrNeighbours + "\n");
    }
    for(int i = 0 ; i < inGraph.length ; i++){
      sb.append(inGraph[i] + " , ");
    }
    sb.append("\n");
    return sb.toString();
  }
}
