import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.Set;
import java.util.List;
import java.util.HashSet;
public class MaxIndSet{
  public int[][] parse(String fileName){
    String fs = File.separator;
    String path = System.getProperty("user.dir") + fs + fileName;
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(new File(path)));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    String currentLine = "";
    int[][] adjMatrix = new int[0][0];
    try {
      currentLine = br.readLine();
      int n = Integer.parseInt(currentLine);
      adjMatrix = new int[n][n];
      int i = 0;
      while ((currentLine = br.readLine()) != null){
        String[] fields = currentLine.split(" ");
        for(int j = 0 ; j < n ; j++){
          adjMatrix[i][j] = Integer.parseInt(fields[j]);
        }
        i++;
      }
    } catch (IOException e1) {
      e1.printStackTrace();
    }
    return adjMatrix;
  }
  public int algorithmR0(int[][] adjMatrix, boolean[] inGraph, int depth){
    String indent = "";
    for(int i = 0 ; i < depth ; i++){
      indent += "\t";
    }
    depth++;

    //System.out.println("depth: " + depth);
    //System.out.println(printMatrix(adjMatrix, indent) + printArray(inGraph, indent));
    //System.out.println();

    if(isEmpty(adjMatrix, inGraph)){
      //System.out.println("Graph empty");
      return 0;
    }
    int nodeWithOneNeigbour = oneNeighbour(adjMatrix);
    if(nodeWithOneNeigbour == 1){ 
      return 1 + algorithmR0(removeAdj(adjMatrix, nodeWithOneNeigbour, inGraph), inGraph, depth);
    }

    int node = isolated(adjMatrix, inGraph);
    if(node != -1){
      //System.out.println("About to go to Isolated");
      return 1 + algorithmR0(remove(adjMatrix, node, inGraph), inGraph, depth);
    }else{
      node = maxDeg(adjMatrix);

      //System.out.println("MaxDegNOde is : " + node);
      //System.out.println("About to go to select or not select node");

      int[][] copyAdjMatrix = copyMatrix(adjMatrix);
      boolean[] copyInGraph = copyArray(inGraph);
      return Math.max(1 + algorithmR0(removeAdj(adjMatrix, node, inGraph), inGraph, depth),
      algorithmR0(remove(copyAdjMatrix, node, copyInGraph), copyInGraph, depth));
    }
  }
  public boolean isEmpty(int[][] adjMatrix, boolean[] inGraph){
    for(int i = 0 ; i < adjMatrix[0].length ; i++){
      if(inGraph[i] == true) return false;
      for(int j = 0 ; j < adjMatrix[0].length ; j++){
        if(adjMatrix[i][j] == 1) return false;
      }
    }
    return true;
  }

  public int oneNeighbour(int[][] adjMatrix){
    for(int i = 0 ; i < adjMatrix[0].length ; i++){
      int sum = 0;
      for(int j = 0 ; j < adjMatrix[0].length ; j++){
        sum += adjMatrix[i][j];
      }
      if(sum == 1) return i;
    }
    return -1;
  }

  public int isolated(int[][] adjMatrix, boolean[] inGraph){
    for(int i = 0 ; i < adjMatrix[0].length ; i++){
      int sum = 0;
        for(int j = 0 ; j < adjMatrix[0].length ; j++){
          sum += adjMatrix[i][j];
        }
        if(sum == 0 && inGraph[i] == true){
          return i;
        }
    }
    return -1;
  }
  public int[][] remove(int[][] adjMatrix, int node, boolean inGraph[]){
    inGraph[node] = false;
    for(int j = 0 ; j < adjMatrix.length ; j++){
      if(adjMatrix[node][j] == 1){
        adjMatrix[node][j] = 0;
        adjMatrix[j][node] = 0;
      }
    }
    return adjMatrix;
  }
  public int[][] removeAdj(int[][] adjMatrix, int maxDegNode, boolean[] inGraph){//i is node we want ot remove
    for(int j = 0 ; j < adjMatrix.length ; j++){
      if(adjMatrix[maxDegNode][j] == 1){
        remove(adjMatrix, j, inGraph);
      }
    }
    remove(adjMatrix, maxDegNode, inGraph);
    return adjMatrix;
  }
  public int[][] copyMatrix(int[][] adjMatrix){
    int[][] copy = new int[adjMatrix.length][adjMatrix.length];
    for(int i = 0 ; i < copy[0].length ; i++){
      for(int j = 0 ; j < copy[0].length ; j++){
        copy[i][j] = adjMatrix[i][j];
      }
    }
    return copy;
  }
  public boolean[] copyArray(boolean[] a){
    boolean[] copy = new boolean[a.length];
    for(int i = 0 ; i < a.length ; i++){
      copy[i] = a[i];
    }
    return copy;
  }
  public int maxDeg(int[][] adjMatrix){
    int max = 0;
    int maxNode = -1;
    for(int i = 0 ; i < adjMatrix[0].length ; i++){
      int sum = 0;
      for(int j = 0 ; j < adjMatrix[0].length ; j++){
        sum += adjMatrix[i][j];
      }
      if(sum > max){
        max = sum;
        maxNode = i;
      }
    }
    return maxNode;
  }
  private String printArray(boolean[] inGraph, String indent){
    StringBuilder sb = new StringBuilder();
    sb.append(indent);
    for(int i = 0 ; i < inGraph.length ; i++){
      sb.append(inGraph[i] + " , ");
    }
    return sb.toString();
  }


  private String printMatrix(int[][] matrix, String indent){
    StringBuilder sb = new StringBuilder();
    for(int i = 0 ; i < matrix[0].length ; i++){
      sb.append(indent);
      for(int j = 0 ; j < matrix[0].length ; j++){
        sb.append(matrix[i][j] + " , ");
      }
      sb.append("\n");
    }
    return sb.toString();
  }

  public static void main(String[] args){
    MaxIndSet mis = new MaxIndSet();
    int[][] adjMatrix = mis.parse("data/g40.in");
    boolean[] inGraph = new boolean[adjMatrix.length];
    for(int i = 0 ; i < inGraph.length ; i++) inGraph[i] = true;
    System.out.println(mis.algorithmR0(adjMatrix, inGraph, 0));
  }
}
