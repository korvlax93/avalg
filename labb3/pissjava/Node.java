import java.util.HashSet;
public class Node implements Comparable<Node>{
  HashSet<Edge> adj;
  int id;
  int degree;
  public Node(int id){
    this.id = id;
    adj = new HashSet<>();
  }
  public void addEdge(Edge e){
    adj.add(e);
  }
  public int compareTo(Node n){
    return this.adj.size() - n.adj.size();
  }
  public String toString(){
    return "" + adj.toString();
  }
  public boolean equals(Object o){
    if (o == this) return true;
    if (!(o instanceof Node)) {
      return false;
    }
    Node n = (Node) o;
    return n.id == id;
  }
}
