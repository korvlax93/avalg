import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.HashSet;
public class MaxIndSet{
  Graph g;
  boolean r1;
  boolean r2;
  int roundCount;
  public MaxIndSet(Graph g, boolean r1, boolean r2){
    this.g = g;
    this.r1 = r1;
    this.r2 = r2;
    roundCount = 0;
  }
  public static Graph parse(String fileName){
    String fs = File.separator;
    String path = System.getProperty("user.dir") + fs + "data" + fs + fileName;
    //System.out.println(path);
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(new File(path)));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    String currentLine = "";
    Graph g = null;
    try {
      currentLine = br.readLine();
      int V = Integer.parseInt(currentLine);
      g = new Graph(V);
      int i = 0;
      while ((currentLine = br.readLine()) != null){
        String[] fields = currentLine.split(" ");
        for(int j = 0 ; j < V ; j++){
          if(Integer.parseInt(fields[j]) == 1) g.addEdge(i, j);
        }
        i++;
      }
    } catch (IOException e1) {
      e1.printStackTrace();
    }
    return g;
  }

  public int R0(ArrayList<Boolean> inGraph){
    roundCount++;
    int emptyCount = 0;
    for(int i = 0 ; i < inGraph.size() ; i++) if(!inGraph.get(i)) emptyCount++;
    if(emptyCount == inGraph.size()) return 0;

    //r1

    if(r1){
      int degOne = g.degreeOne(inGraph);
      if(degOne != -1){
        ArrayList<Integer> addBack = g.removeUAndAdj(degOne, inGraph);
        int degOneRec = 1 + R0(inGraph);
        g.restoreList(inGraph, addBack);
        return degOneRec;
      }
    }





    //r2
    if(r2){
      int degTwo = g.degreeTwo(inGraph);
      if(degTwo != -1){
        int[] neighbours = g.getTwoNeighbours(degTwo, inGraph);
        if(g.hasConnection(neighbours[0], neighbours[1], inGraph)){
          ArrayList<Integer> addBack = g.removeUAndAdj(degTwo, inGraph);
          int degTwoRec = 1 + R0(inGraph);
          g.restoreList(inGraph, addBack);
          return degTwoRec;
        }else{

          int size = g.vertices.size();
          Vertex z = new Vertex(g.vertices.get(size - 1).id + 1);
          int v = degTwo;
          int u = neighbours[0];
          int w = neighbours[1];
          LinkedList<Vertex> uAdj = g.vertices.get(u).adj;
          LinkedList<Vertex> wAdj = g.vertices.get(w).adj;
          LinkedList<Vertex> uwAdjUnion = union(uAdj, wAdj);//this takes too much time?

          g.vertices.add(z);

          for(Vertex a : uwAdjUnion){//Adds all the nodes z should be connected to
            if(a.id != v && inGraph.get(a.id)){
              g.addEdge(z.id, a.id);
              g.addEdge(a.id, z.id);
            }
          }

          inGraph.add(true);
          ArrayList<Integer> addBack = g.removeUAndAdj(degTwo, inGraph);
          int degTwoRec = 1 + R0(inGraph);
          g.restoreList(inGraph, addBack);

          //Remove z
          for(Vertex a : z.adj) a.adj.remove(z);
          g.vertices.remove(z.id);
          inGraph.remove(z.id);

          return degTwoRec;
        }
      }
    }




    int alone = g.isolated(inGraph);
    if(alone != -1){
      int addBack = g.removeIsolated(alone, inGraph);
      int isolate = 1 + R0(inGraph);
      g.restoreInt(inGraph, addBack);
      return isolate;
    }
    else{
      int maxNode = g.findMax(inGraph);

      ArrayList<Integer> addBack = g.removeUAndAdj(maxNode, inGraph);
      int leftMax = 1 + R0(inGraph);
      g.restoreList(inGraph, addBack);

      int addBack2 = g.removeU(maxNode, inGraph);
      int rightMax = R0(inGraph);
      g.restoreInt(inGraph, addBack2);

      return Math.max(leftMax, rightMax);
    }
  }

  public LinkedList<Vertex> union(LinkedList<Vertex> list1, LinkedList<Vertex> list2) {
    HashSet<Vertex> set = new HashSet<Vertex>();

    set.addAll(list1);
    set.addAll(list2);

    return new LinkedList<Vertex>(set);
  }

  public void printArray(boolean[] a){
    for(int i  = 0 ;i < a.length ; i++){
      System.out.print(a[i] + " , ");
    }
    System.out.println();
  }

  public static void main(String[] args){

    /*
    Graph g = parse("g60.in");
    MaxIndSet mis = new MaxIndSet(g, false, false);
    ArrayList<Boolean> inGraph = new ArrayList<>();
    for(int i = 0 ; i < g.vertices.size() ; i++) inGraph.add(true);
    System.out.println("Max int set is " + mis.R0(inGraph));
    */

    System.out.println("R0");
    for(int i = 30 ; i <= 60 ; i+=10){
      Graph g = parse("g" + i + ".in");
      MaxIndSet mis = new MaxIndSet(g, false, false);
      ArrayList<Boolean> inGraph = new ArrayList<>();
      for(int j = 0 ; j < g.vertices.size() ; j++) inGraph.add(true);
      int indmaxset = mis.R0(inGraph);
      System.out.println("File: " + "g" + i + ".in " + "roundCount : " + mis.roundCount + " MaxIndSet: " + indmaxset);
      double c = Math.exp((double)Math.log(mis.roundCount)/(double)i);
      System.out.println("c = " + c);
    }

    System.out.println("R1");
    for(int i = 30 ; i <=100 ; i+=10){
      Graph g = parse("g" + i + ".in");
      MaxIndSet mis = new MaxIndSet(g, true, false);
      ArrayList<Boolean> inGraph = new ArrayList<>();
      for(int j = 0 ; j < g.vertices.size() ; j++) inGraph.add(true);
      int indmaxset = mis.R0(inGraph);
      System.out.println("File: " + "g" + i + ".in " + "roundCount : " + mis.roundCount + " MaxIndSet: " + indmaxset);
      double c = Math.exp((double)Math.log(mis.roundCount)/(double)i);
      System.out.println("c = " + c);
    }
    System.out.println("R2");
    for(int i = 30 ; i <=120 ; i+=10){
      Graph g = parse("g" + i + ".in");
      MaxIndSet mis = new MaxIndSet(g, false, true);
      ArrayList<Boolean> inGraph = new ArrayList<>();
      for(int j = 0 ; j < g.vertices.size() ; j++) inGraph.add(true);
      int indmaxset = mis.R0(inGraph);
      System.out.println("File: " + "g" + i + ".in " + "roundCount : " + mis.roundCount + " MaxIndSet: " + indmaxset);
      double c = Math.exp((double)Math.log(mis.roundCount)/(double)i);
      System.out.println("c = " + c);
    }
  }
}
