import java.util.LinkedList;
import java.util.ArrayList;
public class Vertex{
  int id;
  LinkedList<Vertex> adj;
  int nrNeighbours;
  public Vertex(int id){
    this.id = id;
    adj = new LinkedList<Vertex>();
  }
  public void addNeighbour(int id, ArrayList<Vertex> vertices){
    adj.add(vertices.get(id));
  }
  public int degree(ArrayList<Boolean> inGraph){
    int sum = 0;
    for(Vertex v : adj){
      if(inGraph.get(v.id)){
        sum++;
      }
    }
    return sum;
  }
  public boolean equals(Object o){
    Vertex v = (Vertex)o;
    return v.id == id;
  }
  public String toString(){
    return "" + id;
  }
}
