import java.util.ArrayList;
import java.util.LinkedList;
public class Graph{
  ArrayList<Vertex> vertices;
  public Graph(int V){
    vertices = new ArrayList<Vertex>();
    for(int i = 0 ; i < V ; i++) vertices.add(new Vertex(i));
  }
  public void addEdge(int source, int dest){
    vertices.get(source).addNeighbour(dest, vertices);
  }
  public int isolated(ArrayList<Boolean> inGraph){
    for(int i = 0; i < vertices.size() ; i++){
      Vertex v = vertices.get(i);
      if(inGraph.get(i)){
        int adjSum = 0;
        for(Vertex w : v.adj){
          if(inGraph.get(w.id)){
            adjSum++;
          }
        }
        if(adjSum == 0) return v.id;
      }
    }
    return -1;
  }

  public int degreeOne(ArrayList<Boolean> inGraph){
    for(int i = 0; i < vertices.size() ; i++){
      int adjSum = 0;
      Vertex v = vertices.get(i);
      if(inGraph.get(v.id)){
        for(Vertex w : v.adj){
          if(inGraph.get(w.id)){
            adjSum++;
          }
        }
        if(adjSum == 1) return v.id;
      }
    }
    return -1;
  }

  public int degreeTwo(ArrayList<Boolean> inGraph){
    for(int i = 0; i < vertices.size() ; i++){
      int adjSum = 0;
      Vertex v = vertices.get(i);
      if(inGraph.get(v.id)){
        for(Vertex w : v.adj){
          if(inGraph.get(w.id)){
            adjSum++;
          }
        }
        if(adjSum == 2) return v.id;
      }
    }
    return -1;
  }
  public int[] getTwoNeighbours(int v, ArrayList<Boolean> inGraph){
    int[] neighbours = new int[2];
    int index = 0;
    for(Vertex w : vertices.get(v).adj){
      if(inGraph.get(w.id)){
        neighbours[index++] = w.id;
      }
    }
    return neighbours;
  }
  public boolean hasConnection(int u, int w, ArrayList<Boolean> inGraph){
    Vertex uVertex = vertices.get(u);
    for(Vertex z : uVertex.adj){
      if(inGraph.get(z.id) && z.id == w) return true;
    }
    return false;
  }

  public int findMax(ArrayList<Boolean> inGraph){
    int max = 0;
    int maxNode = -1;
    for(int i = 0 ; i < vertices.size(); i++){
      if(inGraph.get(i)){
        int degree = vertices.get(i).degree(inGraph);
        if(max < degree){
          max = degree;
          maxNode = i;
        }
      }
    }
    return maxNode;
  }

  public int removeIsolated(int isolated, ArrayList<Boolean> inGraph){
    inGraph.set(isolated, false);
    return isolated;
  }

  public int removeU(int u, ArrayList<Boolean> inGraph){
    inGraph.set(u, false);
    return u;
  }
  public ArrayList<Integer> removeUAndAdj(int u, ArrayList<Boolean> inGraph){
    ArrayList<Integer> addBack = new ArrayList<Integer>();
    Vertex uVertex = vertices.get(u);
    inGraph.set(u, false);
    addBack.add(u);
    for(Vertex v : uVertex.adj){
      if(inGraph.get(v.id)){
        inGraph.set(v.id, false);
        addBack.add(v.id);
      }
    }
    return addBack;
  }
  public void restoreListAndZ(ArrayList<Boolean> inGraph, ArrayList<Integer> addBack){
    for(int i = 0 ; i < addBack.size() ; i++)
     inGraph.set(addBack.get(i), true);
  }
  public void restoreList(ArrayList<Boolean> inGraph, ArrayList<Integer> addBack){
    for(int i = 0 ; i < addBack.size() ; i++) inGraph.set(addBack.get(i), true);
  }
  public void restoreInt(ArrayList<Boolean> inGraph, int i){
    inGraph.set(i, true);
  }
  public String toString(ArrayList<Integer> inGraph){
    StringBuilder sb = new StringBuilder();
    for(Vertex v : vertices){
      sb.append("ID: " + v.id + " Adj: " + v.adj + " NrAdj: " + v.nrNeighbours + "\n");
    }
    for(int i = 0 ; i < inGraph.size() ; i++){
      sb.append(inGraph.get(i) + " , ");
    }
    sb.append("\n");
    return sb.toString();
  }
}
