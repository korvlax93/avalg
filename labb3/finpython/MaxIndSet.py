#!/usr/bin/python
import sys

def parse():
    line = sys.stdin.readline()
    N = int(line)
    print(N)
    vertices = list()
    for i in range(N):
        vertices.append([True, set()])
    for i in range(N):
        line = (sys.stdin.readline()).split()
        for j in range(N):
            if(int(line[j])==1):
                vertices[i][1].add(j)
    return vertices
def restore(restore_list):
    for i in restore_list:
        vertices[i][0] = True
def removeUAndAdj(u):
    restore_list = list()

    for i in vertices[u][1]:
        if vertices[i][0]:
            vertices[i][0] = False
            restore_list.append(i)
    vertices[u][0] = False
    restore_list.append(u)
    return restore_list
def R0():

    check = False
    for i in vertices:
        if i[0]==True:
            check = True
            break
    if not check:
        return 0
    max_index = 0
    current_max = 0
    for k in range(len(vertices)):
        i = vertices[k]
        if(i[0] == True):
            if (len(i[1]) == 0):
                i[0] = False;
                choose_isolated = 1+R0()
                restore({k})
                return choose_isolated
            if(len(i[1]) > current_max):
                current_max = len(i[1])
                max_index = k
    restore_list = removeUAndAdj(max_index)
    left_max = 1+R0()
    restore(restore_list)
    vertices[max_index][0] = False
    right_max = R0()
    restore({max_index})
    return max(left_max, right_max)
vertices = parse()
print(R0())
