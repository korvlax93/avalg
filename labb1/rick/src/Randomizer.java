import java.util.Random;


public class Randomizer {
	
	private int size;
	private int[] shuffledArray;
	private int index;
	private Random rnd;
	private int randomMode;
	boolean[] marked;
	
	public Randomizer(int size, boolean[] marked, int randomMode){
		this.size = size;
		this.marked = marked;
		this.randomMode = randomMode;
		index = 0;
		if(randomMode == 1){
			initRandomModeOne();
		}
		else if(randomMode == 2){
			initRandomModeTwo();
		}else if(randomMode == 3){
			initRandomModeThree();
		}

	}

	public int nextInt(){
		int n = -1;
		if(randomMode == 1){
			n = randomOneNextInt();
		}
		else if(randomMode == 2){
			n = randomTwoNextInt();
		}else if(randomMode == 3){
			n = randomThreeNextInt();
		}
		return n;
	}
	
	private void initRandomModeOne(){
		rnd = new Random();
		
	}
	
	private int randomOneNextInt(){
		return rnd.nextInt(size) + 1;
	}
	
	
	private void initRandomModeTwo(){
		shuffledArray = knuthShuffle();
	}
	
	
	private int randomTwoNextInt(){
		return shuffledArray[index++];
	}
	
	private void initRandomModeThree() {
		shuffledArray = knuthShuffle();
	}
	
	private int randomThreeNextInt(){
		int randomInt = 0;
		do{
			randomInt = shuffledArray[index++];
		}while(marked[randomInt]);
		
		
		
		return randomInt;
	}
	
	public int[] knuthShuffle(){
		Random rnd = new Random();
		int[] shuffledArray = new int[size];
		for(int i = 0 ; i < shuffledArray.length; i++){
			shuffledArray[i] = i + 1;
		}
		
		for(int i = shuffledArray.length - 1 ; i >= 1 ; i--){
			int j = rnd.nextInt(size) + 1;
			swap(shuffledArray, i, j - 1);
		}
		return shuffledArray;
	}
	
	public void swap(int[] arr, int i, int j){
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

}
