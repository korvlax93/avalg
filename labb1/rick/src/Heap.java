public class Heap {

	int height;
	int size;
	boolean[] marked;
	int markedCount;
	int randomMode;
	int maxInternalNode;

	public Heap(int height, int randomMode) {
		this.height = height;
		this.randomMode = randomMode;
		size = (int) (Math.pow(2, height) - 1);
		marked = new boolean[size + 1];
		maxInternalNode = size - (size + 1) / 2;
		markedCount = 0;
	}

	public int markTree() {
		Randomizer rnd = new Randomizer(size, marked, randomMode);
		int randomInt = 0;
		int roundCount = 0;
		while (markedCount < size) {
			roundCount++;
			randomInt = rnd.nextInt();
			mark(randomInt);
			/*
			System.out.println("height = " + height + "roundocunt " + roundCount + "randomint " + randomInt);
			System.out.println(printArray(marked));
			*/
		}
		/*
		System.out.println("MarkedCOunt :" + markedCount);
		System.out.println("roundCOunt :" + roundCount);
		*/
		return roundCount;
	}

	public void mark(int i) {
		if (!marked[i]) {
			marked[i] = true;
			markedCount++;
			
			int leftChild = 2 * i;
			int rightChild = 2 * i + 1;
			int sibling;
			int parent;
			
			if (i % 2 == 0) {// if left node
				sibling = i + 1;
				parent = i / 2;
			} else {// if right node
				sibling = i - 1;
				parent = (i - 1) / 2;
			}
			//checks if node is not a leaf
			if (i <= maxInternalNode) {
				if (marked[leftChild] && !marked[rightChild]) {
					mark(rightChild);
				} else if (!marked[leftChild] && marked[rightChild]) {
					mark(leftChild);
				}
			}
			//checks if parent is root
			if (parent != 0) {
				if (marked[sibling] && marked[i]) {// parents left and right children
					mark(parent);
				}
				if(marked[parent] && marked[i]){
					mark(sibling);
				}

			}

		}
	}

	public String printArray(boolean[] arr) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < arr.length; i++) {
			sb.append(arr[i] + ", ");
		}
		sb.append("]");
		return sb.toString();
	}

}
