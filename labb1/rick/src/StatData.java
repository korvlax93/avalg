
public class StatData {
	
	private int sampleSize;
	private int maxHeight;
	private int minHeight;
	private int minRandomMode;
	private int maxRandomMode;
	public StatData(int sampleSize, int minHeight, int maxHeight, int minRandomMode, int maxRandomMode){
		this.sampleSize = sampleSize;
		this.minHeight = minHeight;
		this.maxHeight = maxHeight;
		this.minRandomMode = minRandomMode;
		this.maxRandomMode = maxRandomMode;
	}
	
	public double[] generateSamples(int height, int randomMode){
		double[] samples = new double[sampleSize];
		for(int i = 0 ; i < sampleSize ; i++){
			Heap markingTree = new Heap(height, randomMode);
			samples[i] = markingTree.markTree();
			//System.out.println(samples[i]);
		}
		double max = Integer.MIN_VALUE;
		for(int i = 0 ; i < samples.length ; i++){
			if(samples[i] > max){
				max = samples[i];				
			}
		}
		//System.out.println("max roundcount: " + max);
		
		return samples;
	}
	
	public double expectedValue(double[] samples){
		double sum = 0;
		for(int i = 0 ; i < samples.length ; i++){
			sum += samples[i];
		}
		return sum / (double)samples.length;
	}
	
	public double variance(double[] samples, double expectedValue){
		double sum = 0;
		for(int i = 0 ; i < samples.length ; i++){
			sum += (samples[i] - expectedValue) * (samples[i] - expectedValue);
		}
		return sum / (double)samples.length;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for (int height = minHeight; height <= maxHeight; height++) {
			for(int randomMode = minRandomMode ; randomMode <=maxRandomMode ; randomMode++){
				double[] samples = generateSamples(height, randomMode);
				double expectedValue = expectedValue(samples);
				double variance = variance(samples, expectedValue);
				double standardDev = Math.sqrt(variance);
				double n = Math.pow(2, height) - 1;
				sb.append("RandomMode= " + randomMode + " Height= " + height + " n= " + n  + " -->" + " E = " + expectedValue + " V = " + variance + " D = " + standardDev + "\n");
			}
			sb.append("\n");
		}
		return sb.toString();
	}


}
